﻿namespace MovieMonkey.Models.ApiModels
{
    public class MovieAdListingApiModel
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Id { get; set; }
        public string Type { get; set; }
        public string Poster { get; set; }
    }
}
