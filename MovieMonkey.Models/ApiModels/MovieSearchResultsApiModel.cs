﻿using System.Collections.Generic;

namespace MovieMonkey.Models.ApiModels
{
    public class MovieSearchResultsApiModel
    {
        public IList<MovieAdListingApiModel> Movies { get; set; }
    }
}
