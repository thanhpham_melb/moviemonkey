﻿using System.Collections.Generic;

namespace MovieMonkey.Models.Models
{
    public static class Constants
    {
        public static class SortOption
        {
            public const string PriceAsc = "price";
            public const string PriceDesc = "~price";

            public static Dictionary<string, string> PriceSortOptions = new Dictionary<string, string>
            {
                {"price", "Price Ascending"},
                {"~price", "Price Descending"}
            };
        }

        public static class DisplayOption
        {
            public static Dictionary<int, string> DisplayLimitOptions = new Dictionary<int, string>
            {
                {5, "5 Items"},
                {10, "10 Items"},
                {15, "12 Items"}
            };
        }

        public static class Pagination
        {
            public const int WindowSize = 3;
        }

        public static class SearchOption
        {
            public const int Limit = 5;
            public const int Offset = 0;
        }

        public static class Cookie
        {
            public const string BackToSearchResults = "BackToSearchResults";
        }
    }
}
