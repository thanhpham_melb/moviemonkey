﻿using System.Collections.Generic;

namespace MovieMonkey.Models.Models.Validation
{
    public class ValidationError
    {
        public string Code { get; set; }

        public IList<string> Context { get; set; }

        public string Message { get; set; }

        public ValidationError()
        {
        }

        public ValidationError(string code, string message, IList<string> context)
        {
            Code = code;
            Message = message;
            Context = context;
        }
    }
}
