﻿using System.Collections.Generic;
using System.Linq;

namespace MovieMonkey.Models.Models.Validation
{
    public class ValidationResult
    {
        public ValidationResult() : this(null) { }

        public ValidationResult(params ValidationError[] errors)
        {
            ValidationErrors = errors?.ToList();
        }

        public ValidationResult(IList<ValidationError> errors)
        {
            ValidationErrors = errors;
        }

        public bool IsValid => ValidationErrors == null || !ValidationErrors.Any();

        public IList<ValidationError> ValidationErrors { get; set; }

        public static ValidationResult ValidResult => new ValidationResult();

        public void Add(ValidationResult validationResult)
        {
            foreach (var error in validationResult.ValidationErrors)
            {
                ValidationErrors.Add(error);
            }
        }
    }

    public class ValidationResult<T> : ValidationResult
    {
        public T Result { get; set; }

        public ValidationResult() { }

        public ValidationResult(ValidationResult validationResult)
        {
            ValidationErrors = validationResult.ValidationErrors;
        }
    }
}
