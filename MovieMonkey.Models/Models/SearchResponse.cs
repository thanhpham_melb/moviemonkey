﻿using System.Collections.Generic;

namespace MovieMonkey.Models.Models
{
    public class SearchResponse
    {
        public SearchRequest SearchRequest { get; set; }
        public IList<Movie> SearchResults { get; set; } 
        public long Total { get; set; }
    }
}
