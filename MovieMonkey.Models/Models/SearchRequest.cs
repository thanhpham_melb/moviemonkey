﻿namespace MovieMonkey.Models.Models
{
    public class SearchRequest
    {
        public string Keywords { get; set; }
        public int? Limit { get; set; }
        public int? Offset { get; set; }
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }
        public string Sort { get; set; }
        public string Provider { get; set; }
    }
}
