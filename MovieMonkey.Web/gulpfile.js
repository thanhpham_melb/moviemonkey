'use strict';

var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var concat = require('gulp-concat');
var browserify = require('gulp-browserify');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var pump = require('pump');
var watch = require('gulp-watch');

gulp.task('clean', () => del(['Dist/css/*.*', 'Dist/js/*.*', 'Dist/lib/css/*.*', 'Dist/lib/js/*.*'], {dot: true}));

gulp.task('styles', function() {
    gulp.src('FrontEnd/scss/main.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', function(err){
          return notify().write(err);
        }))
        .pipe(concat('site.css'))
        .pipe(gulp.dest('Dist/css/'));
});

gulp.task('scripts', function(cb){
  pump([
        gulp.src('FrontEnd/js/main.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(browserify({
          insertGlobals: true,
          debug: !gulp.env.production
        }))
        .pipe(concat('site.js')),
        uglify(),
        gulp.dest('Dist/js/')
    ],
    cb
  );
});

gulp.task('fonts', function(){
  gulp.src([
            'FrontEnd/fonts/**/*.*'
          ])
  .pipe(gulp.dest('Dist/fonts/'))
});

gulp.task('icons', function(){
  gulp.src([
            'FrontEnd/icons/**/*.*'
          ])
  .pipe(gulp.dest('Dist/icons/'))
});


gulp.task('images', function(){
  gulp.src([
            'FrontEnd/images/**/*.*'
          ])
  .pipe(gulp.dest('Dist/images/'))
});

gulp.task('css-libs', function(){
  gulp.src([
            'FrontEnd/lib/css/**/*.*'
          ])
  .pipe(concat('libs.css'))
  .pipe(gulp.dest('Dist/lib/css/'))
});

gulp.task('js-libs', function(){
  gulp.src([
            'FrontEnd/lib/js/jquery-1.10.2.min.js',
            'FrontEnd/lib/js/jquery.validate.min.js',
            'FrontEnd/lib/js/jquery.validate.unobtrusive.min.js',
            'FrontEnd/lib/js/bootstrap.min.js',
            'FrontEnd/lib/js/respond.min.js',
            'FrontEnd/lib/js/jquery.dotdotdot.min.js'
          ])
  .pipe(concat('libs.js'))
  .pipe(gulp.dest('Dist/lib/js/'))
});

gulp.task('watch', function() {
  gulp.watch('FrontEnd/scss/**/*.scss', ['styles']);
  gulp.watch('FrontEnd/js/**/*.js', ['scripts']);
});

gulp.task('default', ['clean', 'watch', 'styles', 'scripts', 'js-libs', 'css-libs', 'fonts', 'icons', 'images']);
