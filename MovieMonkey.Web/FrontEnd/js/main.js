var home = require('./home');
var search = require('./search');
var details = require('./details');

home.init();
search.init();
details.init();
