var details = (function() {
  'use strict';
    var init = function() {
        setTimeout(function() {
            console.log('init dotdotdot');
            $('.movie-plot-wrapper').dotdotdot({
                after: 'a.plot-readmore',
                watch: true,
                wrap: 'word',
                callback: function(isTruncated, orgContent) {

                }
            });

            $('a.plot-readmore').on('click', function() {
                $(this).hide();
                var content = $('.movie-plot-wrapper').triggerHandler('originalContent');
                $('.movie-plot-wrapper').empty().append(content).css('height', 'auto');
            });

        }, 10);
    };

    return {
        init: init
    };
})();

module.exports = details;
