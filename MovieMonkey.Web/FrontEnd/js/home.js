var home = (function() {
  'use strict';
    var init = function() {
        $('.overlay-sidebar').on('click', function(e) {
            $('.sidebar-nav').toggleClass('active');
            $('.overlay-cover').css('display', 'block');
        });
        $('.overlay-sidebar-close').on('click', function(e) {
            $('.sidebar-nav').toggleClass('active');
            $('.overlay-cover').css('display', 'none');
        });
    };

    return {
        init: init
    };

})();

module.exports = home;
