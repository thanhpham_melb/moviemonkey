var search = (function() {
'use strict';
    var init = function() {
        $('.search-filter').click(function(e) {
            e.preventDefault();
            $('.refinement').toggleClass('active');
            var chevronIcon = $('.top-bar-secondary i[class*=chevron]');
            if ($('.refinement').hasClass('active')) {
                chevronIcon.removeClass('icon-chevron-right');
                chevronIcon.addClass('icon-chevron-down');
            } else {
                chevronIcon.removeClass('icon-chevron-down');
                chevronIcon.addClass('icon-chevron-right');
            }
        });
    };

    return {
        init: init
    };

})();

module.exports = search;
