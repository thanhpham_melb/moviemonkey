﻿using ClientLibrary.DataAccess.Ioc;
using MovieMonkey.Core.IoC;
using MovieMonkey.Web.ViewModels.Mappers;
using MovieMonkey.Web.ViewModels.MovieDetails.Mappers;
using MovieMonkey.Web.ViewModels.Search.Mappers;
using Ninject.Modules;

namespace MovieMonkey.Web.Ioc
{
    public class WebBindingModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new NinjectModule[]
                        {
                            new CoreBindingModule(),
                            new DataAccessBindingModule()
                        });

            Bind<ISearchRequestMapper>().To<SearchRequestMapper>().InSingletonScope();
            Bind<ISearchResponseMapper>().To<SearchResponseMapper>().InSingletonScope();
            Bind<IPriceViewModelMapper>().To<PriceViewModelMapper>().InSingletonScope();
            Bind<IPaginationViewModelMapper>().To<PaginationViewModelMapper>().InSingletonScope();
            Bind<ISortOptionViewModelMapper>().To<SortOptionViewModelMapper>().InSingletonScope();
            Bind<IDisplayLimitViewModelMapper>().To<DisplayLimitViewModelMapper>().InSingletonScope();
            Bind<IPostbackHelper>().To<PostbackHelper>().InSingletonScope();
            Bind<IMovieDetailsViewModelMapper>().To<MovieDetailsViewModelMapper>().InSingletonScope();
        }
    }
}