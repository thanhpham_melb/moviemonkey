﻿using System.Web.Mvc;
using MovieMonkey.Web.ViewModels.Search;

namespace MovieMonkey.Web.Controllers
{
    public class HomeController : Controller
    {
        [Route]
        public ActionResult Index()
        {
            return View(new SearchQueryViewModel());
        }
        [Route, HttpPost]
        public ActionResult Index(SearchQueryViewModel searchQueryViewModel)
        {
            return Redirect($"/search?&keywords={searchQueryViewModel.Keywords}");
        }
    }
}