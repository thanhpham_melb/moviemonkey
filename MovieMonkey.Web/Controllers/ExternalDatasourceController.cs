﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using MovieMonkey.Core.Services;
using MovieMonkey.Models.Models;
using Ninject.Extensions.Logging;

namespace MovieMonkey.Web.Controllers
{
    public class ExternalDatasourceController : Controller
    {
        private readonly IMovieDataService _movieDataService;
        private readonly ILogger _logger;
        private readonly IExternalDatasourceAggregationService _externalDatasourceAggregationService;
        private readonly IExternalDatasourceService _externalDatasourceService;

        public ExternalDatasourceController(IMovieDataService movieDataService, ILogger logger, 
            IExternalDatasourceAggregationService externalDatasourceAggregationService, 
            IExternalDatasourceService externalDatasourceService)
        {
            _movieDataService = movieDataService;
            _logger = logger;
            _externalDatasourceAggregationService = externalDatasourceAggregationService;
            _externalDatasourceService = externalDatasourceService;
        }

        [Route("externaldatasource/{provider}/movies")]
        public async Task<ActionResult> IndexAsync(string provider)
        {
            var movies = await _externalDatasourceAggregationService.GetMoviesAsync(provider);
            foreach (var movie in movies)
            {
                 var response = await _movieDataService.UpsertAsync(movie);
                _logger.Info($"Upsert response: {response.IsValid}");
            }
            
            return Json(new { results = "OK" }, JsonRequestBehavior.AllowGet);
        }

        [Route("externaldatasource/{provider}/movie/{id}")]
        public async Task<ActionResult> UpsertAsync(string provider, string id)
        {
            var movieApiModel = await _externalDatasourceService.GetDetailsAsync(id, provider);

            var movie = new Movie()
            {
                Poster = movieApiModel.Poster,
                Price = Convert.ToDecimal(movieApiModel.Price),
                Title = movieApiModel.Title,
                Id = movieApiModel.Id,
                Released = movieApiModel.Released,
                Actors = movieApiModel.Actors,
                Provider = provider,
                Year = movieApiModel.Year,
                Language = movieApiModel.Language,
                Writer = movieApiModel.Writer,
                Votes = movieApiModel.Votes,
                Genre = movieApiModel.Genre,
                Director = movieApiModel.Director,
                Status = "Available",
                AttemptCount = 0,
                Plot = movieApiModel.Plot,
                Type = movieApiModel.Type,
                Country = movieApiModel.Country,
                Rating = movieApiModel.Rating,
                Rated = movieApiModel.Rated,
                Metascore = movieApiModel.Metascore
            };

            var response = await _movieDataService.UpsertAsync(movie);
            _logger.Info($"Upsert response: {response.IsValid}");
           
            return Json(new { results = "OK" }, JsonRequestBehavior.AllowGet);
        }
    }
}