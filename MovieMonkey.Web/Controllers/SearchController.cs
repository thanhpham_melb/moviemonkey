﻿using System.Threading.Tasks;
using System.Web.Mvc;
using MovieMonkey.Core.Services;
using MovieMonkey.Models.Models;
using MovieMonkey.Web.ActionFilters;
using MovieMonkey.Web.ViewModels.Mappers;
using MovieMonkey.Web.ViewModels.Search;
using MovieMonkey.Web.ViewModels.Search.Mappers;
using Ninject.Extensions.Logging;

namespace MovieMonkey.Web.Controllers
{
    public class SearchController : Controller
    {
        private readonly ILogger _logger;
        private readonly IMovieDataService _movieDataService;
        private readonly ISearchRequestMapper _searchRequestMapper;
        private readonly ISearchResponseMapper _searchResponseMapper;
        private readonly IPostbackHelper _postbackHelper;

        public SearchController(ILogger logger, IMovieDataService movieDataService, ISearchRequestMapper searchRequestMapper, 
            ISearchResponseMapper searchResponseMapper, IPostbackHelper postbackHelper)
        {
            _logger = logger;
            _movieDataService = movieDataService;
            _searchRequestMapper = searchRequestMapper;
            _searchResponseMapper = searchResponseMapper;
            _postbackHelper = postbackHelper;
        }

        [CookieActionFilter(Name = Constants.Cookie.BackToSearchResults)]
        [Route("search")]
        public async Task<ActionResult> IndexAsync(SearchQueryViewModel searchQueryViewModel)
        {
            var searchResults =
                await _movieDataService.SearchMovieAsync(_searchRequestMapper.Map(searchQueryViewModel));

            return View("~/Views/Search/Index.cshtml", _searchResponseMapper.Map(searchResults));
        }

        [Route("search"), HttpPost]
        public async Task<ActionResult> IndexAsync(SearchResponseViewModel searchResponseViewModel)
        {
            var searchResults =
                await _movieDataService.SearchMovieAsync(_searchRequestMapper.Map(searchResponseViewModel));

            return Redirect(_postbackHelper.GeneratePostbackSearchUrl(searchResponseViewModel.SearchRequest));
        }
    }
}