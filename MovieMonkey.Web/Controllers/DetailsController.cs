﻿using System.Threading.Tasks;
using System.Web.Mvc;
using MovieMonkey.Core.Services;
using MovieMonkey.Web.ViewModels.MovieDetails.Mappers;
using Ninject.Extensions.Logging;

namespace MovieMonkey.Web.Controllers
{
    public class DetailsController : Controller
    {
        private readonly IMovieDataService _movieDataService;
        private readonly ILogger _logger;
        private readonly IMovieDetailsViewModelMapper _movieDetailsViewModelMapper;

        public DetailsController(IMovieDataService movieDataService, ILogger logger, 
            IMovieDetailsViewModelMapper movieDetailsViewModelMapper)
        {
            _movieDataService = movieDataService;
            _logger = logger;
            _movieDetailsViewModelMapper = movieDetailsViewModelMapper;
        }

        [Route("details/{movieId}/{title}")]
        public async Task<ActionResult> IndexAsync(string movieId, string title)
        {
            var movieDetails = await _movieDataService.GetMovieAsync(movieId);
            return View("~/Views/Details/Index.cshtml", _movieDetailsViewModelMapper.Map(movieDetails));
        }
    }
}