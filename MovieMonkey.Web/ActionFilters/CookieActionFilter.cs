﻿using System;
using System.Web;
using System.Web.Mvc;

namespace MovieMonkey.Web.ActionFilters
{
    public class CookieActionFilter : ActionFilterAttribute, IActionFilter
    {
        public string Name { get; set; }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            var response = filterContext.HttpContext.Response;
            var request = filterContext.HttpContext.Request;
            var cookie = new HttpCookie(Name, request.RawUrl)
            {
                Expires = DateTime.Now.AddDays(30)
            };
            response.Cookies.Add(cookie);
        }
    }
}