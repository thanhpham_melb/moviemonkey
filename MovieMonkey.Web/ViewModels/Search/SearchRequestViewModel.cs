﻿using System.Collections.Generic;

namespace MovieMonkey.Web.ViewModels.Search
{
    public class SearchRequestViewModel
    {
        public string Keywords { get; set; }
        public int? Limit { get; set; }
        public int? Offset { get; set; }
        public string MinPrice { get; set; }
        public string MaxPrice { get; set; }
        public string Sort { get; set; }
        public IList<ProviderViewModel> Providers { get; set; } 
    }
}