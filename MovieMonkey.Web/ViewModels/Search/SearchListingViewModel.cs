﻿namespace MovieMonkey.Web.ViewModels.Search
{
    public class SearchListingViewModel
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Rated { get; set; }
        public string Poster { get; set; }
        public string Rating { get; set; }
        public string Id { get; set; }
        public string Price { get; set; }
        public string Runtime { get; set; }
        public string Provider { get; set; }
    }
}