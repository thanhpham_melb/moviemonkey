﻿using System.Collections.Generic;

namespace MovieMonkey.Web.ViewModels.Search
{
    public class SearchResponseViewModel
    {
        public SearchRequestViewModel SearchRequest { get; set; }
        public IList<SearchListingViewModel> SearchResults { get; set; } 
        public PaginationViewModel Pagination { get; set; }
        public SortViewModel SortOption { get; set; }
        public DisplayViewModel DisplayLimit { get; set; }
        public long Total { get; set; }
    }
}