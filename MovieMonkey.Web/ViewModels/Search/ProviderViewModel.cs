﻿namespace MovieMonkey.Web.ViewModels.Search
{
    public class ProviderViewModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool Selected { get; set; }
    }
}