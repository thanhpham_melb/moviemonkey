﻿namespace MovieMonkey.Web.ViewModels.Search
{
    public class SortOptionViewModel
    {
        public string Url { get; set; }
        public string Display { get; set; }
        public bool Selected { get; set; }
    }
}