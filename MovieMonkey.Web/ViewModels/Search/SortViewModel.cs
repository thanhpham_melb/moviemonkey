﻿using System.Collections.Generic;

namespace MovieMonkey.Web.ViewModels.Search
{
    public class SortViewModel
    {
        public IList<SortOptionViewModel> Options { get; set; } 
    }
}