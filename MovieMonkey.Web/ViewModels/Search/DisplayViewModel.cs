using System.Collections.Generic;

namespace MovieMonkey.Web.ViewModels.Search
{
    public class DisplayViewModel
    {
        public IList<DisplayLimitViewModel> DisplayLimits { get; set; } 
    }
}