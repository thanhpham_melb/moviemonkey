namespace MovieMonkey.Web.ViewModels.Search
{
    public class DisplayLimitViewModel
    {
        public string Url { get; set; }
        public string Display { get; set; }
        public bool Selected { get; set; }
    }
}