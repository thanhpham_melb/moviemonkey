﻿using MovieMonkey.Models.Models;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    public interface IPaginationViewModelMapper
    {
        PaginationViewModel Map(SearchResponse searchResponse);
    }
}