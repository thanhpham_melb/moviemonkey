﻿using System.Collections.Generic;
using MovieMonkey.Models.Models;
using MovieMonkey.Web.ViewModels.Mappers;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    class DisplayLimitViewModelMapper : IDisplayLimitViewModelMapper
    {
        private readonly IPostbackHelper _postbackHelper;

        public DisplayLimitViewModelMapper(IPostbackHelper postbackHelper)
        {
            _postbackHelper = postbackHelper;
        }

        public DisplayViewModel Map(SearchRequestViewModel searchRequestViewModel)
        {
            var options = new List<DisplayLimitViewModel>();

            foreach (var displayLimitOption in Constants.DisplayOption.DisplayLimitOptions)
            {
                options.Add(new DisplayLimitViewModel()
                {
                    Selected = displayLimitOption.Key == (searchRequestViewModel.Limit ?? Constants.SearchOption.Limit),
                    Display = displayLimitOption.Value,
                    Url = _postbackHelper.GeneratePostbackSearchUrl(new SearchRequestViewModel()
                    {
                        Offset = Constants.SearchOption.Offset,
                        Providers = searchRequestViewModel.Providers,
                        MinPrice = searchRequestViewModel.MinPrice,
                        MaxPrice = searchRequestViewModel.MaxPrice,
                        Keywords = searchRequestViewModel.Keywords,
                        Limit = displayLimitOption.Key,
                        Sort = searchRequestViewModel.Sort
                    })
                });
            }

            return new DisplayViewModel()
            {
                DisplayLimits = options
            };
        }
    }
}