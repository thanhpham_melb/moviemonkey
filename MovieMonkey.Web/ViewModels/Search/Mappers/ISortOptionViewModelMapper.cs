﻿namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    public interface ISortOptionViewModelMapper
    {
        SortViewModel Map(SearchRequestViewModel searchRequestViewModel);
    }
}