﻿using MovieMonkey.Models.Models;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    public interface ISearchResponseMapper
    {
        SearchResponseViewModel Map(SearchResponse searchResponse);
    }
}