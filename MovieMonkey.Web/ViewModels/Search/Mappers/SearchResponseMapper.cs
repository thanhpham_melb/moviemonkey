﻿using System;
using System.Collections.Generic;
using System.Linq;
using MovieMonkey.Models.Models;
using MovieMonkey.Web.ViewModels.Mappers;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    class SearchResponseMapper : ISearchResponseMapper
    {
        private readonly IPriceViewModelMapper _priceViewModelMapper;
        private readonly IPaginationViewModelMapper _paginationViewModelMapper;
        private readonly ISortOptionViewModelMapper _sortOptionViewModelMapper;
        private readonly IDisplayLimitViewModelMapper _displayLimitViewModelMapper;

        public SearchResponseMapper(IPriceViewModelMapper priceViewModelMapper, 
            IPaginationViewModelMapper paginationViewModelMapper, ISortOptionViewModelMapper sortOptionViewModelMapper,
            IDisplayLimitViewModelMapper displayLimitViewModelMapper)
        {
            _priceViewModelMapper = priceViewModelMapper;
            _paginationViewModelMapper = paginationViewModelMapper;
            _sortOptionViewModelMapper = sortOptionViewModelMapper;
            _displayLimitViewModelMapper = displayLimitViewModelMapper;
        }

        public SearchResponseViewModel Map(SearchResponse searchResponse)
        {
            var providers = new List<ProviderViewModel>()
            {
                new ProviderViewModel()
                {
                    Id = "cinemaworld",
                    Selected = false,
                    Name = "Cinema World"
                },
                new ProviderViewModel()
                {
                    Id = "filmworld",
                    Name = "Film World",
                    Selected = false
                }
            };

            var selectedProvider = providers.FirstOrDefault(x => x.Id == searchResponse.SearchRequest.Provider);

            if (selectedProvider != null)
            {
                selectedProvider.Selected = true;
            }

            var searchRequestViewModel = new SearchRequestViewModel()
            {
                MinPrice = searchResponse.SearchRequest.MinPrice.ToString(),
                MaxPrice = searchResponse.SearchRequest.MaxPrice.ToString(),
                Sort = searchResponse.SearchRequest.Sort,
                Offset = searchResponse.SearchRequest.Offset,
                Limit = searchResponse.SearchRequest.Limit,
                Keywords = searchResponse.SearchRequest.Keywords,
                Providers = providers
            };

            return new SearchResponseViewModel()
            {
                SearchRequest = searchRequestViewModel,
                Total = searchResponse.Total,
                SearchResults = searchResponse.SearchResults.Select(x => new SearchListingViewModel()
                {
                    Title = x.Title,
                    Poster = x.Poster,
                    Price = _priceViewModelMapper.Map(Convert.ToDouble(x.Price)),
                    Id = x.Id,
                    Year = x.Year,
                    Rated = x.Rated,
                    Rating = x.Rating,
                    Runtime = x.Runtime,
                    Provider = x.Provider
                }).ToList(),
                Pagination = _paginationViewModelMapper.Map(searchResponse),
                SortOption = _sortOptionViewModelMapper.Map(searchRequestViewModel),
                DisplayLimit = _displayLimitViewModelMapper.Map(searchRequestViewModel)
            };
        }
    }
}