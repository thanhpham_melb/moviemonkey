using System;
using MovieMonkey.Models.Models;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    class SearchRequestMapper : ISearchRequestMapper
    {
        public SearchRequest Map(SearchQueryViewModel searchQueryViewModel)
        {
            return new SearchRequest()
            {
                MaxPrice = searchQueryViewModel.MaxPrice,
                MinPrice = searchQueryViewModel.MinPrice,
                Provider = searchQueryViewModel.Provider,
                Keywords = searchQueryViewModel.Keywords,
                Offset = searchQueryViewModel.Offset ?? Constants.SearchOption.Offset,
                Limit = searchQueryViewModel.Limit ?? Constants.SearchOption.Limit,
                Sort = searchQueryViewModel.Sort ?? Constants.SortOption.PriceAsc
            };
        }

        public SearchRequest Map(SearchResponseViewModel searchResponseViewModel)
        {
            return new SearchRequest()
            {
                MaxPrice = Convert.ToDouble(searchResponseViewModel.SearchRequest.MaxPrice),
                MinPrice = Convert.ToDouble(searchResponseViewModel.SearchRequest.MinPrice),
                Provider = null,
                Keywords = searchResponseViewModel.SearchRequest.Keywords,
                Offset = searchResponseViewModel.SearchRequest.Offset,
                Limit = searchResponseViewModel.SearchRequest.Limit,
                Sort = searchResponseViewModel.SearchRequest.Sort
            };
        }
    }
}