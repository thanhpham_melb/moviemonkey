﻿namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    public interface IDisplayLimitViewModelMapper
    {
        DisplayViewModel Map(SearchRequestViewModel searchRequestViewModel);
    }
}