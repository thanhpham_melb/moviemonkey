using System.Collections.Generic;
using System.Linq;
using MovieMonkey.Models.Models;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    class PaginationViewModelMapper : IPaginationViewModelMapper
    {
        public PaginationViewModel Map(SearchResponse searchResponse)
        {
            var pageCount = (searchResponse.Total + searchResponse.SearchRequest.Limit - 1) /
                            searchResponse.SearchRequest.Limit;

            var currentPage = searchResponse.SearchRequest.Offset / searchResponse.SearchRequest.Limit + 1;

            if (currentPage < 1)
            {
                currentPage = 1;
            }
            else if (currentPage > pageCount)
            {
                currentPage = (int?)pageCount;
            }

            var pages = new List<PageViewModel>();
            var pageWindow = new List<PageViewModel>();

            var goPreviousUrl = GetGoPreviousUrl(searchResponse.SearchRequest);
            var goNextUrl = GetGoNextUrl(searchResponse.SearchRequest);

            for (var page = 1; page <= pageCount; page++)
            {
                pages.Add(new PageViewModel()
                {
                    PageNumber = page,
                    Selected = page == currentPage,
                    PageUrl =
                        page == currentPage
                            ? string.Empty
                            : GetGotoPageUrl(searchResponse.SearchRequest, page),
                    Display =
                        $"{page*searchResponse.SearchRequest.Limit - searchResponse.SearchRequest.Limit + 1} - {page*searchResponse.SearchRequest.Limit}"
                });
            }
            
            if (currentPage == 1)
            {
                pageWindow.AddRange(pages.Take(Constants.Pagination.WindowSize));
            }
            else if (currentPage == pageCount)
            {
                pageWindow.AddRange(
                    pages.Skip((int)(pageCount - Constants.Pagination.WindowSize))
                        .Take(Constants.Pagination.WindowSize));
            }
            else
            {
                pageWindow.AddRange(
                    pages.Skip((int)(currentPage - (Constants.Pagination.WindowSize / 2 + 1)))
                        .Take(Constants.Pagination.WindowSize));
            }

            return new PaginationViewModel()
            {
                Pages = pages,
                PageWindow = pageWindow,
                GoPreviousUrl = goPreviousUrl,
                GoNextUrl = goNextUrl
            };
        }

        private string GetGoPreviousUrl(SearchRequest searchRequest)
        {
            return GetSequentialNavUrl(searchRequest, false);
        }

        private string GetGoNextUrl(SearchRequest searchRequest)
        {
            return GetSequentialNavUrl(searchRequest, true);
        }

        private string GetGotoPageUrl(SearchRequest searchRequest, int page)
        {
            var limit = searchRequest.Limit.HasValue ? $"&limit={searchRequest.Limit.Value}" : string.Empty;
            var offset = searchRequest.Offset.HasValue ? $"&offset={(page - 1) * searchRequest.Limit.Value}" : string.Empty;
            var sort = !string.IsNullOrEmpty(searchRequest.Sort) ? $"&sort={searchRequest.Sort}" : string.Empty;
            var keywords = !string.IsNullOrEmpty(searchRequest.Keywords)
                ? $"&keywords={searchRequest.Keywords}"
                : string.Empty;
            var minPrice = (searchRequest.MinPrice.HasValue) ? $"&minprice={searchRequest.MinPrice}" : string.Empty;
            var maxPrice = (searchRequest.MaxPrice.HasValue) ? $"&maxprice={searchRequest.MaxPrice}" : string.Empty;

            var provider = !string.IsNullOrEmpty(searchRequest.Provider) ? $"&provider={searchRequest.Provider}" : string.Empty;

            return $"/search?{limit}{offset}{sort}{keywords}{minPrice}{maxPrice}{provider}";
        }

        private string GetSequentialNavUrl(SearchRequest searchRequest, bool isGoNext)
        {
            var limit = searchRequest.Limit.HasValue ? $"&limit={searchRequest.Limit.Value}" : string.Empty;
            var sort = !string.IsNullOrEmpty(searchRequest.Sort) ? $"&sort={searchRequest.Sort}" : string.Empty;
            var keywords = !string.IsNullOrEmpty(searchRequest.Keywords)
                ? $"&keywords={searchRequest.Keywords}"
                : string.Empty;
            var minPrice = (searchRequest.MinPrice.HasValue) ? $"&minprice={searchRequest.MinPrice}" : string.Empty;
            var maxPrice = (searchRequest.MaxPrice.HasValue) ? $"&maxprice={searchRequest.MaxPrice}" : string.Empty;

            var provider = !string.IsNullOrEmpty(searchRequest.Provider) ? $"&provider={searchRequest.Provider}" : string.Empty;

            string offset;

            if (isGoNext)
            {
                offset = searchRequest.Offset.HasValue
                    ? $"&offset={searchRequest.Offset.Value + searchRequest.Limit.Value}"
                    : string.Empty;
            }
            else
            {
                offset = searchRequest.Offset.HasValue
                    ? $"&offset={searchRequest.Offset.Value - searchRequest.Limit.Value}"
                    : string.Empty;
            }

            return $"/search?{limit}{offset}{sort}{keywords}{minPrice}{maxPrice}{provider}";
        }
    }
}