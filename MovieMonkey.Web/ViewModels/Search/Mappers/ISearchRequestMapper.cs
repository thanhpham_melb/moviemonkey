﻿using MovieMonkey.Models.Models;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    public interface ISearchRequestMapper
    {
        SearchRequest Map(SearchQueryViewModel searchQueryViewModel);
        SearchRequest Map(SearchResponseViewModel searchResponseViewModel);
    }
}