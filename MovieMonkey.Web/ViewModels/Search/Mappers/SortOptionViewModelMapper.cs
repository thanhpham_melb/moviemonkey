﻿using System.Collections.Generic;
using MovieMonkey.Models.Models;
using MovieMonkey.Web.ViewModels.Mappers;

namespace MovieMonkey.Web.ViewModels.Search.Mappers
{
    class SortOptionViewModelMapper : ISortOptionViewModelMapper
    {
        private readonly IPostbackHelper _postbackHelper;

        public SortOptionViewModelMapper(IPostbackHelper postbackHelper)
        {
            _postbackHelper = postbackHelper;
        }

        public SortViewModel Map(SearchRequestViewModel searchRequestViewModel)
        {
            var options = new List<SortOptionViewModel>();

            foreach (var priceSortOption in Constants.SortOption.PriceSortOptions)
            {
                options.Add(new SortOptionViewModel()
                {
                    Selected = priceSortOption.Key == searchRequestViewModel.Sort,
                    Display = priceSortOption.Value,
                    Url = _postbackHelper.GeneratePostbackSearchUrl(new SearchRequestViewModel()
                    {
                        Offset = Constants.SearchOption.Offset,
                        Providers = searchRequestViewModel.Providers,
                        MinPrice = searchRequestViewModel.MinPrice,
                        MaxPrice = searchRequestViewModel.MaxPrice,
                        Keywords = searchRequestViewModel.Keywords,
                        Limit = searchRequestViewModel.Limit,
                        Sort = priceSortOption.Key
                    })
                });
            }

            return new SortViewModel()
            {
                Options = options
            };
        }
    }
}