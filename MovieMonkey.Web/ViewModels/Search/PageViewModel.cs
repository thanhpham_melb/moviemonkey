namespace MovieMonkey.Web.ViewModels.Search
{
    public class PageViewModel
    {
        public bool Selected { get; set; }
        public string Display { get; set; }
        public int PageNumber { get; set; }
        public string PageUrl { get; set; }
    }
}