﻿using System.Collections.Generic;
using System.Linq;

namespace MovieMonkey.Web.ViewModels.Search
{
    public class PaginationViewModel
    {
        public IList<PageViewModel> Pages { get; set; }
        public IList<PageViewModel> PageWindow { get; set; }
        public string GoNextUrl { get; set; }
        public string GoPreviousUrl { get; set; }
        
        public long PageCount => Pages?.Count ?? 0;

        public bool CanGoPrevious
        {
            get { return Pages != null && Pages.Any(x => x.PageNumber == 1 && !x.Selected); }
        }

        public bool CanGoNext
        {
            get { return Pages != null && Pages.Any(x => x.PageNumber == PageCount && !x.Selected); }
        }
    }
}