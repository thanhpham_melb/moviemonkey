﻿using MovieMonkey.Models.Models;

namespace MovieMonkey.Web.ViewModels.MovieDetails.Mappers
{
    public interface IMovieDetailsViewModelMapper
    {
        MovieViewModel Map(Movie movie);
    }
}