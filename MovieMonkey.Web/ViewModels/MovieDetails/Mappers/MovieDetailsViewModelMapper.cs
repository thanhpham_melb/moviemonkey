﻿using System;
using MovieMonkey.Core.Services;
using MovieMonkey.Models.Models;
using MovieMonkey.Web.ViewModels.Mappers;

namespace MovieMonkey.Web.ViewModels.MovieDetails.Mappers
{
    public class MovieDetailsViewModelMapper : IMovieDetailsViewModelMapper
    {
        private readonly IPriceViewModelMapper _priceViewModelMapper;
        private readonly ICookieService _cookieService;

        public MovieDetailsViewModelMapper(IPriceViewModelMapper priceViewModelMapper, ICookieService cookieService)
        {
            _priceViewModelMapper = priceViewModelMapper;
            _cookieService = cookieService;
        }

        public MovieViewModel Map(Movie movie)
        {
            return new MovieViewModel()
            {
                MovieDetails = new MovieDetailsViewModel()
                {
                    Provider = movie.Provider,
                    Id = movie.Id,
                    Price = _priceViewModelMapper.Map(Convert.ToDouble(movie.Price)),
                    Poster = movie.Poster,
                    Title = movie.Title,
                    Year = movie.Year,
                    Rating = movie.Rating,
                    Rated = movie.Rated,
                    Released = movie.Released,
                    Actors = movie.Actors,
                    Language = movie.Language,
                    Writer = movie.Writer,
                    Votes = movie.Votes,
                    Genre = movie.Genre,
                    Director = movie.Director,
                    Plot = movie.Plot,
                    Type = movie.Type,
                    Country = movie.Country,
                    Metascore = movie.Metascore,
                    Runtime = movie.Runtime
                },
                BackToSearchResultsUrl = _cookieService.GetValue(Constants.Cookie.BackToSearchResults)
            };
        }
    }
}