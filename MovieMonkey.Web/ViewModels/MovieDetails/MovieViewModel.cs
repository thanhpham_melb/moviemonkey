﻿namespace MovieMonkey.Web.ViewModels.MovieDetails
{
    public class MovieViewModel
    {
        public MovieDetailsViewModel MovieDetails { get; set; }
        public string BackToSearchResultsUrl { get; set; }
    }
}