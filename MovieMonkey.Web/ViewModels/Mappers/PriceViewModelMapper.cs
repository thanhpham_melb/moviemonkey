namespace MovieMonkey.Web.ViewModels.Mappers
{
    class PriceViewModelMapper : IPriceViewModelMapper
    {
        public string Map(double price)
        {
            return $"{price:C}";
        }
    }
}