﻿using MovieMonkey.Web.ViewModels.Search;

namespace MovieMonkey.Web.ViewModels.Mappers
{
    public interface IPostbackHelper
    {
        string GeneratePostbackSearchUrl(SearchRequestViewModel searchRequest);
    }
}