﻿namespace MovieMonkey.Web.ViewModels.Mappers
{
    public interface IPriceViewModelMapper
    {
        string Map(double price);
    }
}