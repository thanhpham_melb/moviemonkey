﻿using System.Linq;
using MovieMonkey.Web.ViewModels.Search;

namespace MovieMonkey.Web.ViewModels.Mappers
{
    class PostbackHelper : IPostbackHelper
    {
        public string GeneratePostbackSearchUrl(SearchRequestViewModel searchRequest)
        {
            var limit = searchRequest.Limit.HasValue ? $"&limit={searchRequest.Limit.Value}" : string.Empty;
            var offset = searchRequest.Offset.HasValue ? $"&offset={searchRequest.Offset.Value}" : string.Empty;
            var sort = !string.IsNullOrEmpty(searchRequest.Sort) ? $"&sort={searchRequest.Sort}" : string.Empty;
            var keywords = !string.IsNullOrEmpty(searchRequest.Keywords)
                ? $"&keywords={searchRequest.Keywords}"
                : string.Empty;
            var minPrice = !string.IsNullOrEmpty(searchRequest.MinPrice) ? $"&minprice={searchRequest.MinPrice}" : string.Empty;
            var maxPrice = !string.IsNullOrEmpty(searchRequest.MaxPrice) ? $"&maxprice={searchRequest.MaxPrice}" : string.Empty;

            var provider = string.Empty;
            if (searchRequest.Providers != null &&
                (searchRequest.Providers.Count(x => x.Selected)) == 1)
            {
                provider = $"&provider={searchRequest.Providers.First(x => x.Selected).Id}";
            }

            return $"/search?{limit}{offset}{sort}{keywords}{minPrice}{maxPrice}{provider}";
        }
    }
}