﻿using System;
using MovieMonkey.Core.Services;
using MovieMonkey.Models.Models;
using MovieMonkey.Web.ViewModels.Mappers;
using MovieMonkey.Web.ViewModels.MovieDetails.Mappers;
using NUnit.Framework;
using Rhino.Mocks;

namespace MovieMonkey.Test.Mappers
{
    [TestFixture]
    class MovieDetailsViewModelMapperTests
    {
        private IPriceViewModelMapper _priceViewModelMapper;
        private ICookieService _cookieService;
        private MovieDetailsViewModelMapper _movieDetailsViewModelMapper;


        [SetUp]
        public void Init()
        {
            _priceViewModelMapper = MockRepository.GenerateMock<IPriceViewModelMapper>();
            _cookieService = MockRepository.GenerateMock<ICookieService>();
            _movieDetailsViewModelMapper = new MovieDetailsViewModelMapper(_priceViewModelMapper, _cookieService);
        }

        [Test]
        public void Map_ValidMovie_ShouldReturnViewModel()
        {
            var movie = new Movie()
            {
                Id = "abc123",
                Price = (decimal) 19.95
            };

            _priceViewModelMapper.Expect(x => x.Map(Convert.ToDouble(movie.Price))).Return("$19.95");
            _cookieService.Stub(x => x.GetValue(Constants.Cookie.BackToSearchResults)).Return("http://abc.123.com");

            var movieViewModel = _movieDetailsViewModelMapper.Map(movie);
            Assert.AreEqual(movieViewModel.MovieDetails.Price, "$19.95");
            Assert.AreEqual(movieViewModel.BackToSearchResultsUrl, "http://abc.123.com");
        }
    }
}
