﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientLibrary.DataAccess.Core;
using MovieMonkey.Core.Services;
using MovieMonkey.Models.Models;
using Ninject.Extensions.Logging;
using NUnit.Framework;
using Rhino.Mocks;


namespace MovieMonkey.Test.Services
{
    [TestFixture]
    class MovieDataServiceTest
    {
        private ILogger _logger;
        private IMovieDataAccessService _movieDataAccessService;
        private MovieDataService _movieDataService;
        [SetUp]
        public void Init()
        {
            _logger = MockRepository.GenerateMock<ILogger>();
            _movieDataAccessService = MockRepository.GenerateMock<IMovieDataAccessService>();
            _movieDataService = new MovieDataService(_movieDataAccessService, _logger);
        }

        [Test]
        public async void GetMovieAsync_IdFound_ShouldReturnMovie()
        {
            var movieId = "abc123";

            _movieDataAccessService.Expect(x => x.GetMovieAsync(movieId))
                .Return(Task.FromResult(new Movie()
                {
                    Id = "abc123"
                }));

            var movie = await _movieDataService.GetMovieAsync(movieId);

            Assert.IsNotNull(movie);
            Assert.AreEqual(movieId, movie.Id);
        }

        [Test]
        [ExpectedException(typeof(Exception))]
        public async void GetMovieAsync_IdNotFound_ShouldThrowException()
        {
            var movieId = "abc123";

            _movieDataAccessService.Expect(x => x.GetMovieAsync(movieId))
                .Throw(new Exception("Movie not found"));

            var movie = await _movieDataService.GetMovieAsync(movieId);
        }

        [Test]
        public async void SearchMovieAync_KeywordFound_ShouldReturnMovies()
        {
            var searchRequest = new SearchRequest()
            {
                Keywords = "star wars",
                Limit = 10,
                Offset = 0,
                Sort = "price"
            };

            _movieDataAccessService.Expect(x => x.SearchMoviesAsync(searchRequest))
                .Return(Task.FromResult(new SearchResponse()
                {
                    SearchResults = new List<Movie>()
                    {
                        new Movie()
                        {
                            Title = "Movie star wars 1"
                        },
                        new Movie()
                        {
                            Title = "Movie star wars 2"
                        }
                    },
                    Total = 2
                }));

            var movies = await _movieDataService.SearchMovieAsync(searchRequest);
            Assert.IsNotNull(movies);
            Assert.Greater(movies.Total, 0);
            Assert.IsTrue(movies.SearchResults.Any(x => x.Title.Contains("star wars")));
        }
    }
}
