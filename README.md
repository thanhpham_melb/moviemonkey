# Introduction #
This project provides users a tool to search cheapest movies from different providers. Be aware of that there will be assumptions. It's written in ASP.NET MVC and 100% hosted on AWS. See below for required implementation details.

### DevOps ###
* An AWS EC2 t2.micro instance to host the web app.
* An AWS ELB to increase the workload and reliability of the application.
* An Elasticsearch service for indexing and searching movies

### Back-End ###
* C# & ASP.NET MVC
* Async programming & micro services approaches for improving user experience
* NLog for logging and error handling
* NUnit & RhinoMocks for unit testing
* SOLID & DRY principles as a requirement for code quality and readability

### Front-End ###
* Bootstrap for UI responsive across multiple devices
* NPM & Gulp for compiling & minifying SASS, Jquery, Javascript, fonts, images, etc

## Assumptions ##
1. Because the external WebAPI which returns movies is so flaky and unreliable so instead of fetching data for the application directly, the AWS Elasticsearch will be used as a substitute. There will be a constant background service which periodically fetches data from the external WebAPI and update indices in AWS Elasticasearch. I assume this is already done by using [Hangfire](http://hangfire.io/) component.
2. I hard coded image urls on the search result and detail pages to avoid broken UI. This is because image urls (which pointing to imdb resource) returned from the external WebAPI throws 403 Forbidden exception most of the time.
3. The AWS Elasticsearch service has public access at the moment, however this should be restricted on production environment. This can be retrieved by restricting access to authorised AWS accounts only and then exposing custom Web APIs on top of the ES to public.

# Installation #
1. Clone the repo at [here](https://thanhpham_melb@bitbucket.org/thanhpham_melb/moviemonkey.git)
2. Open the solution file using Visual Studio 2015
3. Rebuild the project - this should restore all Nuget packages
4. If you want to update front end UI or Javascript, run 'gulp' from the command line to compile all front end assets