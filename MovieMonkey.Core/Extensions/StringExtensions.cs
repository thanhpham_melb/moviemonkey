﻿using System.Text.RegularExpressions;

namespace MovieMonkey.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ToSeoUrl(this string url, bool lower = false)
        {
            var encodedUrl = (url ?? "");

            if (lower)
                encodedUrl = encodedUrl.ToLower();

            // replace & with and
            encodedUrl = Regex.Replace(encodedUrl, @"\&+", "and");

            // remove characters
            encodedUrl = encodedUrl.Replace("'", "");

            // remove invalid characters
            encodedUrl = Regex.Replace(encodedUrl, @"[^a-zA-Z0-9_]", "-");

            // remove duplicates
            encodedUrl = Regex.Replace(encodedUrl, @"-+", "-");

            // trim leading & trailing characters
            encodedUrl = encodedUrl.Trim('-');

            return encodedUrl;
        }

    }
}
