﻿using System.Configuration;
using ClientLibrary.WebApi;
using MovieMonkey.Core.Services;
using Ninject.Modules;

namespace MovieMonkey.Core.IoC
{
    public class CoreBindingModule : NinjectModule
    {
        private readonly string _externalDatasourceServiceUrl;

        public CoreBindingModule()
        {
            _externalDatasourceServiceUrl = ConfigurationManager.AppSettings["External.Datasource.Service.Url"] ??
                                            "http://webjetapitest.azurewebsites.net";
        }
        public override void Load()
        {
            Bind<IExternalDatasourceService>().To<ExternalDatasourceService>().InSingletonScope();
            Bind<IWebApi>()
                .To<WebApi>()
                .WhenInjectedExactlyInto<ExternalDatasourceService>()
                .InSingletonScope()
                .WithConstructorArgument("apiUrl", _externalDatasourceServiceUrl);

            Bind<IExternalDatasourceAggregationService>().To<ExternalDatasourceAggregationService>().InSingletonScope();
            Bind<IMovieDataService>().To<MovieDataService>().InSingletonScope();
            Bind<ICookieService>().To<CookieService>().InSingletonScope();
        }
    }
}
