﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieMonkey.Models.Models;
using Ninject.Extensions.Logging;

namespace MovieMonkey.Core.Services
{
    class ExternalDatasourceAggregationService : IExternalDatasourceAggregationService
    {
        private readonly IExternalDatasourceService _externalDatasourceService;
        private readonly ILogger _logger;

        public ExternalDatasourceAggregationService(IExternalDatasourceService externalDatasourceService, ILogger logger)
        {
            _externalDatasourceService = externalDatasourceService;
            _logger = logger;
        }

        public async Task<IList<Movie>> GetMoviesAsync(string provider)
        {
            var aggregateMovies = new List<Movie>();
            var movies = await _externalDatasourceService.GetResultsAsync(provider);

            foreach (var movie in movies.Movies)
            {
                try
                {
                    var movieDetails = await _externalDatasourceService.GetDetailsAsync(movie.Id, provider);
                    aggregateMovies.Add(new Movie()
                    {
                        Id = movieDetails.Id,
                        Poster = movieDetails.Poster,
                        Price = Convert.ToDecimal(movieDetails.Price),
                        Rated = movieDetails.Rated,
                        Rating = movieDetails.Rating,
                        Released = movieDetails.Released,
                        Title = movieDetails.Title,
                        Type = movieDetails.Type,
                        Year = movieDetails.Year,
                        Actors = movieDetails.Actors,
                        AttemptCount = 0,
                        Country = movieDetails.Country,
                        Director = movieDetails.Director,
                        Genre = movieDetails.Genre,
                        Language = movieDetails.Language,
                        Metascore = movieDetails.Metascore,
                        Plot = movieDetails.Plot,
                        Provider = provider,
                        Status = "Available",
                        Votes = movieDetails.Votes,
                        Writer = movieDetails.Writer,
                        Runtime = movieDetails.Runtime
                    });
                }
                catch (Exception exception)
                {
                    _logger.Error($"Could not read movie details from external source: {exception.StackTrace}");
                }

            }

            return aggregateMovies;
        }
    }
}
