﻿using System.Threading.Tasks;
using ClientLibrary.DataAccess.Core;
using MovieMonkey.Models.Models;
using MovieMonkey.Models.Models.Validation;
using Ninject.Extensions.Logging;

namespace MovieMonkey.Core.Services
{
    public class MovieDataService : IMovieDataService
    {
        private readonly IMovieDataAccessService _movieDataAccessService;
        private readonly ILogger _logger;

        public MovieDataService(IMovieDataAccessService movieDataAccessService, ILogger logger)
        {
            _movieDataAccessService = movieDataAccessService;
            _logger = logger;
        }

        public async Task<ValidationResult> UpsertAsync(Movie movie)
        {
            return await _movieDataAccessService.UpsertMovieAsync(movie);
        }

        public async Task<SearchResponse> SearchMovieAsync(SearchRequest searchRequest)
        {
            return await _movieDataAccessService.SearchMoviesAsync(searchRequest);
        }

        public async Task<Movie> GetMovieAsync(string id)
        {
            return await _movieDataAccessService.GetMovieAsync(id);
        }
    }
}