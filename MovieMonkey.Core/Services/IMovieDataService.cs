﻿using System.Threading.Tasks;
using MovieMonkey.Models.Models;
using MovieMonkey.Models.Models.Validation;

namespace MovieMonkey.Core.Services
{
    public interface IMovieDataService
    {
        Task<ValidationResult> UpsertAsync(Movie movie);
        Task<SearchResponse> SearchMovieAsync(SearchRequest searchRequest);
        Task<Movie> GetMovieAsync(string id);
    }
}
