﻿using System.Web;
using MovieMonkey.Models.Models;

namespace MovieMonkey.Core.Services
{
    class CookieService : ICookieService
    {
        public string GetValue(string cookieName)
        {
            var request = HttpContext.Current.Request;
            if (request.UrlReferrer != null && request.Cookies[Constants.Cookie.BackToSearchResults] != null)
            {
                return request.Cookies[Constants.Cookie.BackToSearchResults].Value;
            }
            return string.Empty;
        }
    }
}