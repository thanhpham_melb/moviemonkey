﻿namespace MovieMonkey.Core.Services
{
    public interface ICookieService
    {
        string GetValue(string cookieName);
    }
}
