﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MovieMonkey.Models.Models;

namespace MovieMonkey.Core.Services
{
    public interface IExternalDatasourceAggregationService
    {
        Task<IList<Movie>> GetMoviesAsync(string provider);
    }
}