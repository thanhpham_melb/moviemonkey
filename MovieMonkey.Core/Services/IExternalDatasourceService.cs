﻿using System.Threading.Tasks;
using MovieMonkey.Models.ApiModels;

namespace MovieMonkey.Core.Services
{
    public interface IExternalDatasourceService
    {
        Task<MovieSearchResultsApiModel> GetResultsAsync(string provider);
        Task<MovieApiModel> GetDetailsAsync(string id, string provider);
    }
}
