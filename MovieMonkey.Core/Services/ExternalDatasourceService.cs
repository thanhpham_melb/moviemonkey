﻿using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ClientLibrary.WebApi;
using MovieMonkey.Models.ApiModels;
using Ninject.Extensions.Logging;

namespace MovieMonkey.Core.Services
{
    class ExternalDatasourceService : IExternalDatasourceService
    {
        private readonly ILogger _logger;
        private readonly IWebApi _webApi;
        private readonly WebHeaderCollection _headers;

        public ExternalDatasourceService(IWebApi webApi, ILogger logger)
        {
            _logger = logger;
            _webApi = webApi;
            _headers = new WebHeaderCollection()
            {
                {
                    "x-access-token",
                    ConfigurationManager.AppSettings["x-access-token"]
                }
            };
        }

        public async Task<MovieSearchResultsApiModel> GetResultsAsync(string provider)
        {
            return
                await
                    _webApi.GetAsync<MovieSearchResultsApiModel>($"/api/{provider}/movies", HttpMethod.Get, null,
                        _headers);
        }

        public async Task<MovieApiModel> GetDetailsAsync(string id, string provider)
        {
            return await _webApi.GetAsync<MovieApiModel>($"/api/{provider}/movie/{id}", HttpMethod.Get, null, _headers);
        }
    }
}