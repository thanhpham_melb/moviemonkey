using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ninject.Extensions.Logging;

namespace ClientLibrary.WebApi
{
    public class WebApi : IWebApi
    {
        private readonly string _apiUrl;
        private readonly ILogger _logger;

        private readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            NullValueHandling =
                NullValueHandling.Ignore,
            PreserveReferencesHandling =
                PreserveReferencesHandling.Objects,
            TypeNameHandling = TypeNameHandling.Auto,
            TypeNameAssemblyFormat =
                FormatterAssemblyStyle.Simple
        };

        private readonly HttpClient _client;

        public WebApi(string apiUrl, ILogger logger)
        {
            _apiUrl = apiUrl;
            _logger = logger;
            _client = new HttpClient()
            {
                BaseAddress = new Uri(_apiUrl)
            };

            //Note: Some servers do not handle properly the Expect header which might cause timed out errors so disabling this will avoid those issues. In fact this can increase the performance by reducing a round trip.
            _client.DefaultRequestHeaders.ExpectContinue = false;

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task PostAsync(string requestUrl, HttpMethod method, object postData = null, WebHeaderCollection headerCollection = null)
        {
            await PostRequestAsync(requestUrl, method, postData, headerCollection);
        }

        public async Task<T> GetAsync<T>(string requestUrl, HttpMethod method, object postData = null,
            WebHeaderCollection headerCollection = null)
        {
            return await PostRequestAsync<T>(requestUrl, method, postData, headerCollection);
        }

        #region Private methods
        private async Task<HttpResponseMessage> PostRequestAsync(string requestUrl, HttpMethod method, object postData = null, WebHeaderCollection headerCollection = null)
        {
            try
            {
                var requestMessage = new HttpRequestMessage(method, requestUrl);

                if (headerCollection != null)
                {
                    foreach (var header in headerCollection.AllKeys)
                    {
                        requestMessage.Headers.Add(header, headerCollection[header]);
                    }
                }

                if (postData != null)
                {
                    var serializedObject = JsonConvert.SerializeObject(postData, _jsonSerializerSettings);
                    var httpContent = new StringContent(serializedObject, Encoding.UTF8, "application/json");
                    requestMessage.Content = httpContent;
                }

                var response = await _client.SendAsync(requestMessage).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return response;
                }

                throw new SimpleHttpResponseException()
                {
                    HttpResponseMessage = response
                };
            }
            catch (HttpRequestException httpRequestException)
            {
                _logger.Error(httpRequestException, "Request url: {0}, Post data: {1}, Header collections: {2}",
                    requestUrl, postData, headerCollection);
                throw;
            }
            catch (Exception exception)
            {
                _logger.Error(exception, "Request url: {0}, Post data: {1}, Header collections: {2}", requestUrl,
                    postData, headerCollection);
                throw;
            }
        }

        private async Task<T> PostRequestAsync<T>(string requestUrl, HttpMethod method, object postData = null, WebHeaderCollection headerCollection = null)
        {
            var response = await PostRequestAsync(requestUrl, method, postData, headerCollection).ConfigureAwait(false);

            //Make sure the request returns a successful code, otherwise it will throw exception 
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();

            return !string.IsNullOrEmpty(content) ? JsonConvert.DeserializeObject<T>(content, _jsonSerializerSettings) : default(T);
        }
        #endregion
    }
}