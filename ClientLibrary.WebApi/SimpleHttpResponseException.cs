﻿using System;
using System.Net.Http;

namespace ClientLibrary.WebApi
{
    class SimpleHttpResponseException : Exception
    {
        public HttpResponseMessage HttpResponseMessage { get; set; }
    }
}