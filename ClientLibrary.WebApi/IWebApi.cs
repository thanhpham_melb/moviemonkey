﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientLibrary.WebApi
{
    public interface IWebApi
    {
        Task PostAsync(string requestUrl, HttpMethod method, object postData = null, WebHeaderCollection headerCollection = null);
        Task<T> GetAsync<T>(string requestUrl, HttpMethod method, object postData = null, WebHeaderCollection headerCollection = null);

    }
}
