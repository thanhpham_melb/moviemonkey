﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieMonkey.Models.Models;
using MovieMonkey.Models.Models.Validation;
using Nest;
using Ninject.Extensions.Logging;
using SearchRequest = MovieMonkey.Models.Models.SearchRequest;

namespace ClientLibrary.DataAccess.Core.Elasticsearch
{
    public class ElasticsearchMovieDataAccessService : IMovieDataAccessService
    {
        private readonly ILogger _logger;
        private readonly IElasticClient _elasticClient;

        public ElasticsearchMovieDataAccessService(ILogger logger, IElasticClient elasticClient)
        {
            _logger = logger;
            _elasticClient = elasticClient;
        }

        public async Task<ValidationResult> UpsertMovieAsync(Movie movie)
        {
            try
            {
                var response = await _elasticClient.UpdateAsync(new DocumentPath<Movie>(movie), x => x.Index("movies").Doc(movie).DocAsUpsert());

                var validation = new ValidationResult(new List<ValidationError>());

                if (!response.IsValid)
                {
                    validation.ValidationErrors.Add(new ValidationError()
                    {
                        Message = response.Result.ToString()
                    });
                }

                return validation;
            }
            catch (Exception exception)
            {
                _logger.Error($"Upsert movie exception: {exception.StackTrace}");
                throw;
            }
        }

        public async Task<ValidationResult> DeleteMovieAsync(string id)
        {
            try
            {
                var response = await _elasticClient.DeleteAsync(new DocumentPath<Movie>(id));

                var validation = new ValidationResult(new List<ValidationError>());

                if (!response.IsValid)
                {
                    validation.ValidationErrors.Add(new ValidationError()
                    {
                        Message = response.Result.ToString()
                    });
                }

                return validation;
            }
            catch (Exception exception)
            {
                _logger.Error($"Delete movie exception: {exception.StackTrace}");
                throw;
            }
        }

        public async Task<SearchResponse> SearchMoviesAsync(SearchRequest searchRequest)
        {
            try
            {
                var response = await _elasticClient.SearchAsync<Movie>(x => x
                            .Size(searchRequest.Limit ?? 10)
                            .From(searchRequest.Offset ?? 0)
                            .Index("movies")
                            .Query(q => q
                                .MultiMatch(mm => mm
                                    .Fields(f => f.Field(fld => fld.Title).Field(fld => fld.Plot).Field(fld => fld.Genre))
                                    .Query(searchRequest.Keywords)) && q
                                .Bool(b => b.Must(bm => bm.Match(m => m.Field(mf => mf.Status).Query("Available")))) && q
                                .Bool(b =>
                                {
                                    if (!string.IsNullOrEmpty(searchRequest.Provider))
                                    {
                                        return b.Must(bm => bm.Match(m => m.Field(mf => mf.Provider).Query(searchRequest.Provider)));
                                    }
                                    return null;
                                }) && q
                                .Range(r =>
                                {
                                    if (searchRequest.MinPrice.HasValue && searchRequest.MaxPrice.HasValue)
                                    {
                                        return r.Field(fld => fld.Price).GreaterThanOrEquals(searchRequest.MinPrice.Value).LessThanOrEquals(searchRequest.MaxPrice.Value);
                                    }
                                    if (searchRequest.MinPrice.HasValue)
                                    {
                                        return r.Field(fld => fld.Price).GreaterThanOrEquals(searchRequest.MinPrice.Value);
                                    }
                                    if (searchRequest.MaxPrice.HasValue)
                                    {
                                        return r.Field(fld => fld.Price).LessThanOrEquals(searchRequest.MaxPrice.Value);
                                    }
                                    return null;

                                }))
                            .Sort(s =>
                            {
                                if (string.IsNullOrEmpty(searchRequest.Sort) || searchRequest.Sort == Constants.SortOption.PriceAsc)
                                {
                                    return s.Ascending(a => a.Price);
                                }
                                return s.Descending(a => a.Price);
                            }));

                var searchResponse = new SearchResponse()
                {
                    SearchResults = response.Hits.Select(hit => hit.Source).ToList(),
                    Total = response.Total,
                    SearchRequest = searchRequest
                };

                return searchResponse;
            }
            catch (Exception exception)
            {
                _logger.Error($"Search movie exception: {exception.StackTrace}");
                throw;
            }

        }

        public async Task<Movie> GetMovieAsync(string id)
        {
            try
            {
                var response = await _elasticClient.GetAsync(new DocumentPath<Movie>(id), x => x.Index("movies"));
                return response.Source;
            }
            catch (Exception exception)
            {
                _logger.Error($"Get movie exception: {exception.StackTrace}");
                throw;
            }
        }
    }
}
