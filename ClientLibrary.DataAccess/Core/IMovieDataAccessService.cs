﻿using System.Threading.Tasks;
using MovieMonkey.Models.Models;
using MovieMonkey.Models.Models.Validation;

namespace ClientLibrary.DataAccess.Core
{
    public interface IMovieDataAccessService
    {
        Task<ValidationResult> UpsertMovieAsync(Movie movie);
        Task<ValidationResult> DeleteMovieAsync(string id);
        Task<SearchResponse> SearchMoviesAsync(SearchRequest searchRequest);
        Task<Movie> GetMovieAsync(string id);
    }
}
