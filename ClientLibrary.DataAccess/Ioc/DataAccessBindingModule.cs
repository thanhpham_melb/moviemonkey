﻿using System;
using System.Configuration;
using ClientLibrary.DataAccess.Core;
using ClientLibrary.DataAccess.Core.Elasticsearch;
using Elasticsearch.Net;
using Nest;
using Ninject.Modules;

namespace ClientLibrary.DataAccess.Ioc
{
    public class DataAccessBindingModule : NinjectModule
    {
        private readonly ConnectionSettings _connectionSettings;

        public DataAccessBindingModule()
        {
            var elasticsearchNodeUri = ConfigurationManager.AppSettings["ElasticsearchNodeUri"];
            var nodes = new[]
            {
                new Uri(elasticsearchNodeUri)
            };
            var pool = new StaticConnectionPool(nodes);
            _connectionSettings = new ConnectionSettings(pool);
        }

        public override void Load()
        {
            Bind<IMovieDataAccessService>().To<ElasticsearchMovieDataAccessService>().InSingletonScope();
            Bind<IElasticClient>().ToMethod(x => new ElasticClient(_connectionSettings)).InSingletonScope();
        }
    }
}
